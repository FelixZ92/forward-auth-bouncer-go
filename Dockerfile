FROM alpine
RUN mkdir /application

ADD build/forward-auth-bouncer-go /application/forward-auth-bouncer-go

ENTRYPOINT [ "/application/forward-auth-bouncer-go" ]
