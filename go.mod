module gitlab.com/felixz92/forward-auth-bouncer-go

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/go-resty/resty/v2 v2.3.0
	github.com/lestrrat-go/jwx v1.0.4
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/tools v0.0.0-20200914161755-17fc728d0d1e // indirect
)
