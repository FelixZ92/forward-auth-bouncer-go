package config

import (
	"log"
	"os"
	"strconv"
)

// Config holds our configuration for obtaining a jwks
type Config struct {
	JwksEndpoint     string
	JwtVerifyIssuer  string
	JwksURIIgnoreSSl bool
	CookieName       string
}

// ReadConfig initializes a Config object
func ReadConfig() Config {
	return Config{
		JwksEndpoint:     lookupJwksEndpoint(),
		JwtVerifyIssuer:  lookupJwtVerifyIssuer(),
		JwksURIIgnoreSSl: lookupJwksURIIgnoreSSl(),
		CookieName:       lookupCookieName(),
	}
}

func lookupCookieName() string {
	jwksEndpoint, found := os.LookupEnv("COOKIE_NAME")
	if !found {
		log.Printf("COOKIE_NAME not set, defaulting to kc-access")
		return "kc-access"
	}
	return jwksEndpoint
}

func lookupJwksEndpoint() string {
	jwksEndpoint, found := os.LookupEnv("JWKS_ENDPOINT")
	if !found {
		panic("JWKS_ENDPOINT not set")
	}
	return jwksEndpoint
}

func lookupJwtVerifyIssuer() string {
	jwtIssuer, found := os.LookupEnv("JWT_VERIFY_ISSUER")
	if !found {
		panic("JWT_VERIFY_ISSUER not set")
	}
	return jwtIssuer
}

func lookupJwksURIIgnoreSSl() bool {
	ignore, found := os.LookupEnv("JWKS_URI_IGNORE_SSL")
	if !found {
		log.Printf("JWKS_URI_IGNORE_SSL not set, defaulting to false")
		return false
	}
	ignoreSsl, err := strconv.ParseBool(ignore)
	if err != nil {
		panic("JWKS_URI_IGNORE_SSL is not a bool")
	}
	return ignoreSsl
}
