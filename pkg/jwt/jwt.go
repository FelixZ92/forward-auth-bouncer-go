package jwt

import (
	"crypto/tls"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/go-resty/resty/v2"
	"github.com/lestrrat-go/jwx/jwk"
	"github.com/lestrrat-go/jwx/jwt"
	"gitlab.com/felixz92/forward-auth-bouncer-go/pkg/config"
	"log"
	"net/http"
	"runtime"
	"strings"
	"time"
)

// AuthRequired is a gin-gonic middleware requiring a jwt to be present in auth header or cookie
func AuthRequired() gin.HandlerFunc {
	cfg := config.ReadConfig()
	jwks := initJwkls(cfg)
	log.Printf("jwks loaded")
	return func(c *gin.Context) {
		var encodedJwt string
		cookie, err := fetchEncodedJwtFromCookie(c, cfg.CookieName)
		if cookie == "" || err != nil {
			log.Printf("cookie not found, lookup Authorization Header")
			encodedJwt, err = fetchEncodedJwtFromAuthorizationHeader(c)
			if err != nil {
				_ = c.AbortWithError(http.StatusUnauthorized, err)
				return
			}
		} else {
			encodedJwt = cookie
		}

		token, err := verifyJwt(c, encodedJwt, jwks, cfg.JwtVerifyIssuer)

		if err != nil {
			_ = c.AbortWithError(http.StatusUnauthorized, err)
			return
		}
		groups, found := token.Get("k8s")

		if !found {
			_ = c.AbortWithError(http.StatusUnauthorized, errors.New("k8s claim not found"))
			return
		}

		c.Set("jwt", token)
		c.Set("k8s", groups)
	}
}

func verifyJwt(c *gin.Context, encodedJwt string, jwks *jwk.Set, issuer string) (jwt.Token, error) {
	token, err := jwt.ParseString(encodedJwt, jwt.WithKeySet(jwks))

	if err != nil || token == nil {
		log.Print(err)
		return nil, err
	}
	log.Printf("token: %v", token)
	err = jwt.Verify(token,
		jwt.WithIssuer(issuer),
		jwt.WithAudience("k8s"),
	)

	if err != nil {
		log.Print(err)
		return nil, err
	}

	return token, nil
}

func fetchEncodedJwtFromAuthorizationHeader(c *gin.Context) (string, error) {
	const prefix = "Bearer "
	authHeader := c.Request.Header.Get("Authorization")
	if len(authHeader) < len(prefix) || !strings.EqualFold(authHeader[:len(prefix)], prefix) {
		return "", errors.New("authorization header not set")
	}
	return authHeader[len(prefix):], nil
}

func fetchEncodedJwtFromCookie(c *gin.Context, cookieName string) (string, error) {
	cookie, err := c.Request.Cookie(cookieName)
	if err != nil {
		return "", err
	}
	if cookie != nil {
		return cookie.Value, nil
	}
	return "", errors.New("cookie not set")
}

func initJwkls(config config.Config) *jwk.Set {
	client := resty.New()
	if config.JwksURIIgnoreSSl {
		client.SetTransport(&http.Transport{
			Proxy:                 http.ProxyFromEnvironment,
			ForceAttemptHTTP2:     true,
			MaxIdleConns:          100,
			IdleConnTimeout:       90 * time.Second,
			TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
			ExpectContinueTimeout: 1 * time.Second,
			MaxIdleConnsPerHost:   runtime.GOMAXPROCS(0) + 1,
		})
	}

	resp, err := client.R().
		EnableTrace().
		Get(config.JwksEndpoint)

	if err != nil {
		panic(err)
	}

	jwks, err := jwk.ParseBytes(resp.Body())
	if err != nil {
		panic(err)
	}
	return jwks
}
