package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/felixz92/forward-auth-bouncer-go/pkg/jwt"
	"log"
	"reflect"
)

func main() {

	r := gin.Default()
	r.Use(gin.Logger())
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	//r.GET("/keys", func(context *gin.Context) {
	//	context.JSON(200, jwt.Jwks)
	//})

	r.GET("/dashboard/", func(context *gin.Context) {
		context.Redirect(303, "/dashboard/")
	})

	authorization := r.Group("/auth")
	authorization.Use(jwt.AuthRequired())
	{
		authorization.GET("/admin", adminEndpoint)
		authorization.GET("/readonly", readonlyEndpoint)
	}

	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}

func readonlyEndpoint(context *gin.Context) {
	groups, found := context.Get("k8s")
	if !found || !hasGroup(groups, "cluster-readonly") {
		context.JSON(401, gin.H{
			"authorized": "false",
		})
		return
	}
	context.JSON(200, gin.H{
		"authorized": "true",
	})
}

func adminEndpoint(context *gin.Context) {
	groups, found := context.Get("k8s")
	log.Printf("groups: %v", groups)
	if !found || !hasGroup(groups, "cluster-admin") {
		context.JSON(401, gin.H{
			"authorized": "false",
		})
		return
	}
	context.JSON(200, gin.H{
		"authorized": "true",
	})
}

func hasGroup(groups interface{}, group string) bool {
	switch reflect.TypeOf(groups).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(groups)
		for i := 0; i < s.Len(); i++ {
			if s.Index(i).Interface() == group {
				return true
			}
		}
		return false
	}
	return false
}
